﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyRates.ViewModels
{
    public class CurrencyViewModel
    {
        public string Currency { get; set; }
        public double SellCash { get; set; }
        public double BuyCash { get; set; }
    }
}