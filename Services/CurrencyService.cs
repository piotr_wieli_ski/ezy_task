﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using CurrencyRates.Interfaces;
using CurrencyRates.ViewModels;

namespace CurrencyRates.Services
{
    public class CurrencyService : ICurrencyService
    {
        public void GetCurrencyRates(ref List<CurrencyViewModel> list)
        {
            string url = Consts.URLConsts.CurrencyRateSiteURL;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(url);
            XmlElement root = xmlDocument.DocumentElement;
            XmlNodeList noodes = root?.SelectNodes("//web_dis_rates/row");
            foreach (XmlNode node in noodes)
            {
                var swiftCode = node["swift_code"].InnerText;
                if (swiftCode == "EUR" || swiftCode == "USD")
                {
                    CurrencyViewModel currency = new CurrencyViewModel()
                    {
                        Currency = node["swift_name"].InnerText,
                        BuyCash = Convert.ToDouble(node["buy_cash"].InnerText),
                        SellCash = Convert.ToDouble(node["sell_cash"].InnerText)
                    };

                    list.Add(currency);
                }

            }
           
        }
    }
}