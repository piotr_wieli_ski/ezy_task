﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CurrencyRates.ViewModels;

namespace CurrencyRates.Interfaces
{
    public interface ICurrencyService
    {
        void GetCurrencyRates(ref List<CurrencyViewModel> list);
    }
}