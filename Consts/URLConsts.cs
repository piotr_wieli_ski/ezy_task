﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyRates.Consts
{
    public static class URLConsts
    {
        public const string CurrencyRateSiteURL = "http://www.forex.se/ratesxml.asp?id=492";
    }
}